# 📖 阅读｜书源分享

##  :fa-th-list: 更新公告
- 修复书源
- 替换失效书源
- 修复订阅源
##  :fa-stack-overflow: 3.0书源
- 书源：`270个书源`
- [3.0书源链接](http://shuyuan.miaogongzi.net/shuyuan/1627741986.json)
- [书源下载地址](https://wwi.lanzoui.com/b05hl9sf)
- 更新日期：2021年07月30日（星期五）
##  :fa-rss: 订阅源
- 订阅源：`203个订阅源`
- [订阅源链接](http://shuyuan.miaogongzi.net/shuyuan/1627682222.json)
- [订阅下载地址](https://wwi.lanzoui.com/b05hl9sf)
- 更新日期：2021年07月30日（星期五）
##  :fa-arrows: 导入方案
- 建议删除旧书源，从新导入最新书源。
##  :fa-film: 导入教程
#### 书源导入教程
1. 复制上面发布的“3.0书源”
2. 打开阅读APP
3. 在“我的”一栏选择“书源管理”
4. 点击右上角三个点
5. 选择“网络导入”
6. 粘贴链接点确定
7. 弹出书源确定对话框，如果显示未全选，请先全选再点确定，因为有些源的规则有修改
#### 订阅源导入教程
1. 下载上面发布的“订阅源”
2. 打开阅读APP
3. 在“订阅”一栏选择右上角“:fa-cog:”图标
4. 点击右上角三个点
5. 选择“本地导入”
6. 找到并选择下载好的文件
7. 弹出订阅源确定对话框，如果显示未全选，请先全选再点确定，因为有些源的规则有修改
##   :fa-share-alt: 主页导航
- [开源阅读](https://github.com/gedoor/legado/releases/)（阅读APP）
- [源仓库](http://yck.mumuceo.com/)（书源网站）
- [网站挂了吗](https://gualemang.com/)（链接检测）
##  :fa-lightbulb-o: 温馨提示
- 本站所有内容仅供书友交流学习，勿做商用。